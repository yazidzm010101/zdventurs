package com.example.zdventurs;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.zdventurs.helpers.AuthHelper;
import com.example.zdventurs.helpers.BookingHelper;
import com.example.zdventurs.models.Booking;
import com.example.zdventurs.models.User;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class BookingActivity extends AppCompatActivity {
    private ArrayList<String> city;
    private ArrayList<int[]> prices;
    private AutoCompleteTextView drDeparture;
    private AutoCompleteTextView drDestination;
    private TextInputEditText edAdults;
    private TextInputEditText edChilds;
    private TextInputEditText edDate;
    private TextInputEditText edTime;
    private Calendar calendar;
    private FloatingActionButton fabCreate;
    private int price = 0;
    private float childRate = 0.6f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);
        getSupportActionBar().setTitle("Booking");

        drDeparture = (AutoCompleteTextView) findViewById(R.id.departure);
        drDestination = (AutoCompleteTextView) findViewById(R.id.destination);
        edAdults = (TextInputEditText) findViewById(R.id.adults);
        edChilds = (TextInputEditText) findViewById(R.id.childs);
        edDate = (TextInputEditText) findViewById(R.id.date);
        edTime = (TextInputEditText) findViewById(R.id.datetime);
        fabCreate = (FloatingActionButton) findViewById(R.id.create);

        setCity();
        setPrice();
        updateCity(drDeparture);
        updateCity(drDestination);
        pairCityLisener(drDeparture, drDestination);
        pairCityLisener(drDestination, drDeparture);
        validateNumber(edAdults);
        validateNumber(edChilds);
        listenDate(edDate);
        listenTime(edTime);
        handleCreate(fabCreate);
    }

    private void setCity() {
        this.city = new ArrayList<String>();
        city.add("Jakarta");
        city.add("Surabaya");
        city.add("Bandung");
    }

    private void setPrice() {
        this.prices = new ArrayList<>();
        prices.add(new int[]{0, 120000, 100000});
        prices.add(new int[]{120000, 0, 100000});
        prices.add(new int[]{100000, 100000, 0});
    }

    private void pairCityLisener(AutoCompleteTextView drCity, AutoCompleteTextView drOtherCity) {
        drCity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                validateCity(drOtherCity, drCity);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                validateCity(drCity, drOtherCity);
                updatePrice();
            }
        });
    }

    private void validateCity(AutoCompleteTextView drCity, AutoCompleteTextView drOtherCity) {
        String current = drCity.getText().toString();
        ArrayList<String> cityChanged = new ArrayList<String>(city);
        int index = cityChanged.indexOf(current);
        if (index >= 0) {
            cityChanged.remove(index);
            updateCity(cityChanged, drOtherCity);
        }
    }

    private void validateNumber(TextInputEditText tx) {
        tx.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                updatePrice();
            }
        });
    }

    private void updateCity(ArrayList<String> city, AutoCompleteTextView drCity) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this,
                R.layout.dropdown_menu_popup_city,
                city
        );
        drCity.setAdapter(adapter);
    }

    private void updateCity(AutoCompleteTextView drCity) {
        updateCity(this.city, drCity);
    }

    private void updatePrice() {
        String departure = drDeparture.getText().toString();
        String destination = drDestination.getText().toString();
        int indexDeparture = city.indexOf(departure);
        int indexDestination = city.indexOf(destination);
        if (indexDeparture >= 0 && indexDestination >= 0) {
            TextView txPrice = (TextView) findViewById(R.id.price);
            TextInputEditText edAdults = (TextInputEditText) findViewById(R.id.adults);
            TextInputEditText edChilds = (TextInputEditText) findViewById(R.id.childs);
            int currentPrice = prices.get(indexDeparture)[indexDestination];
            int totalAdults = 0;
            int totalChilds = 0;

            try {
                totalAdults = Integer.parseInt(edAdults.getText().toString());
            } catch (NumberFormatException ignore) {
            }

            try {
                totalChilds = Integer.parseInt(edChilds.getText().toString());
            } catch (NumberFormatException ignore) {
            }

            price = (currentPrice * totalAdults) + (int) (currentPrice * childRate * totalChilds);
            String txPriceText = "Rp. " + price;
            txPrice.setText(txPriceText);
        }
    }

    private void listenDate(TextInputEditText dateView) {
        calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        updateDate(year, month + 1, day);
        dateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker(year, month, day);
            }
        });
    }

    private void listenTime(TextInputEditText timeView) {
        SimpleDateFormat sdfHour = new SimpleDateFormat("HH");
        SimpleDateFormat sdfMinute = new SimpleDateFormat("mm");
        calendar = Calendar.getInstance();

        int hour = Integer.parseInt(sdfHour.format(calendar.getTime()));
        int minute = Integer.parseInt(sdfMinute.format(calendar.getTime()));
        updateTime(hour, minute);
        timeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePicker(hour, minute);
            }
        });
    }

    private void showDatePicker(int year, int month, int day) {
        DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker arg0,
                                  int arg1, int arg2, int arg3) {
                updateDate(arg1, arg2 + 1, arg3);
            }
        };
        DatePickerDialog datePicker = new DatePickerDialog(BookingActivity.this,
                datePickerListener, year, month, day);
        datePicker.show();
    }

    private void showTimePicker(int hour, int minute) {
        TimePickerDialog.OnTimeSetListener timeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                updateTime(hourOfDay, minute);
            }
        };
        TimePickerDialog timePickerDialog = new TimePickerDialog(BookingActivity.this,
                timeSetListener, hour, minute, true
        );
        timePickerDialog.show();
    }

    private void updateDate(int year, int month, int day) {
        edDate.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
    }

    private void updateTime(int hour, int minute) {
        edTime.setText(String.format("%02d:%02d", hour, minute));
    }

    private void handleCreate(FloatingActionButton fabCreate) {
        fabCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    AuthHelper authHelper = new AuthHelper(BookingActivity.this);
                    BookingHelper bookingHelper = new BookingHelper(BookingActivity.this);
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                    Date timestamp = (Date) sdf.parse(edDate.getText().toString() + " " + edTime.getText().toString());
                    User user = authHelper.details();

                    Booking booking = new Booking(
                            0,
                            user.getId(),
                            drDeparture.getText().toString(),
                            drDestination.getText().toString(),
                            timestamp.getTime(),
                            Integer.parseInt(edAdults.getText().toString()),
                            Integer.parseInt(edChilds.getText().toString()),
                            price
                    );

                    if (bookingHelper.create(booking)) {
                        Toast.makeText(BookingActivity.this, "Berhasil membuat booking", Toast.LENGTH_LONG).show();
                        finish();
                    } else {
                        throw new Error();
                    }
                } catch (Exception e) {
                    Toast.makeText(BookingActivity.this, "Gagal membuat booking!", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

}
