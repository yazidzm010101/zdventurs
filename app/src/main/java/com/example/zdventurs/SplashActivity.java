package com.example.zdventurs;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.example.zdventurs.helpers.AuthHelper;

public class SplashActivity extends AppCompatActivity {
    AuthHelper authHelper;
    Class action;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        authHelper = new AuthHelper(SplashActivity.this);

        if(authHelper.validateSession()) {
            action = DashboardActivity.class;
        } else {
            action = LoginActivity.class;
        }

        Intent intent = new Intent(SplashActivity.this, action);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

}