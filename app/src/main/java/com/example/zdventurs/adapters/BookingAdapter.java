package com.example.zdventurs.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.zdventurs.R;
import com.example.zdventurs.helpers.BookingHelper;
import com.example.zdventurs.models.Booking;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class BookingAdapter extends ArrayAdapter<Booking> {
    public BookingAdapter(Activity context, ArrayList<Booking> bookings) {
        super(context, 0, bookings);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item_booking, parent, false);
        }


        Booking current = getItem(position);
        TextView txDay = listItemView.findViewById(R.id.day);
        TextView txMonth = listItemView.findViewById(R.id.month);
        TextView txTrip = listItemView.findViewById(R.id.trip);
        TextView txDesc = listItemView.findViewById(R.id.desc);
        TextView txPrice = listItemView.findViewById(R.id.price);
        TextView txTime = listItemView.findViewById(R.id.time);
        FloatingActionButton fabDelete = listItemView.findViewById(R.id.delete);

        SimpleDateFormat sfDay = new SimpleDateFormat("dd");
        SimpleDateFormat sfMonth = new SimpleDateFormat("MMM");
        SimpleDateFormat sfTime = new SimpleDateFormat("HH:mm");
        Date currentDate = new Date(current.getTimestamp());
        String id = String.valueOf(current.getId());
        String trip = current.getDeparture().concat(" - ").concat(current.getDestination());
        String price = String.format("Rp. %d", current.getPrice());
        String desc = String.format("%d Dewasa", current.getAdults());
        desc = String.format("%s • %d Anak", desc, current.getChilds());

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
        params.weight = 1.0f;

        txDay.setText(sfDay.format(currentDate));
        txMonth.setText(sfMonth.format(currentDate));

        txTrip.setBackground(null);

        txTrip.setText(trip);
        txDesc.setText(desc);
        txPrice.setText(price);
        txTime.setText(sfTime.format(currentDate));

        fabDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog deleteDialog = buildDeleteDialog(fabDelete, id, position);
                deleteDialog.show();
            }
        });

        return listItemView;
    }

    public Dialog buildDeleteDialog(FloatingActionButton fabDelete, String id, int position) {
        BookingHelper bookingHelper = new BookingHelper(getContext());
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Yakin ingin menghapus data ini?")
                .setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(bookingHelper.delete(id)) {
                            remove(getItem(position));
                            Toast.makeText(getContext(), "Berhasil menghapus", Toast.LENGTH_LONG).show();
                        }else {
                            Toast.makeText(getContext(), "Gagal menghapus", Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // batal
                    }
                });
        return builder.create();
    }
}
