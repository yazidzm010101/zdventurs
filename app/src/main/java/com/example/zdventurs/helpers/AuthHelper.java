package com.example.zdventurs.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;

import com.example.zdventurs.models.User;

import org.mindrot.jbcrypt.BCrypt;

import java.util.Calendar;


public class AuthHelper extends DatabaseHelper {
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    final String TABLE_NAME = "users";
    final String PREF_NAME = "SessionPref";
    final String KEY_TOKEN = "remember_token";
    final int PRIVATE_MODE = 0;

    public AuthHelper(Context ctx) {
        super(ctx);
        pref = ctx.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public boolean create(User user) {
        boolean isSuccess = false;
        ContentValues values = new ContentValues();
        values.put("name", user.getName());
        values.put("username", user.getUsername());
        values.put("password", BCrypt.hashpw(user.getPassword(), BCrypt.gensalt()));

        db = openDB();
        if (db.insert(TABLE_NAME, null, values) > 0) {
            isSuccess = true;
            rememberSession(user.getUsername());
        }
        db.close();

        return isSuccess;
    }

    public User details() {
        String remember_token = pref.getString(KEY_TOKEN, "");
        String[] columns = {"id", "name", "username"};
        String selection = "remember_token=?";
        String[] selectionArgs = {remember_token};
        User user = new User(0, null, null, null);

        db = openDB();
        cursor = db.query(TABLE_NAME, columns, selection, selectionArgs, null, null, null);
        while (cursor.getCount() > 0 && cursor.moveToNext()) {
            user = new User(
                    cursor.getLong(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    null
            );
        }
        cursor.close();
        db.close();

        return user;
    }

    public boolean update(User user) {
        String whereClause = "id=?";
        String[] whereArgs = {String.valueOf(user.getId())};
        ContentValues values = new ContentValues();
        values.put("name", user.getName());
        values.put("username", user.getUsername());
        values.put("password", BCrypt.hashpw(user.getPassword(), BCrypt.gensalt()));

        db = openDB();
        long id = db.update(TABLE_NAME, values, whereClause, whereArgs);
        db.close();
        return id > 0;
    }

    public boolean attemptLogin(String username, String password) {
        boolean isSuccess = false;
        String[] columns = {"username, password"};
        String selection = "username=?";
        String[] selectionArgs = {username};

        db = openDB();
        cursor = db.query(TABLE_NAME, columns, selection, selectionArgs, null, null, null);
        while (cursor.getCount() > 0 && cursor.moveToNext()) {
            isSuccess = BCrypt.checkpw(password, cursor.getString(1));
            rememberSession(username);
        }
        db.close();

        return isSuccess;
    }

    public void attemptlogout() {
        // get remember_token dari pref
        String remember_token = pref.getString(KEY_TOKEN, "");

        if (!remember_token.isEmpty()) {
            String selection = "remember_token=?";
            String[] selectionArgs = {remember_token};
            ContentValues values = new ContentValues();
            values.put(KEY_TOKEN, "");

            editor.putString(KEY_TOKEN, null);
            editor.commit();

            db = openDB();
            db.update(TABLE_NAME, values, selection, selectionArgs);
            db.close();
        }
    }

    public void rememberSession(String username) {
        ContentValues values = new ContentValues();
        String date = Calendar.getInstance().getTime().toString();
        String hashToken = BCrypt.hashpw(date, BCrypt.gensalt());
        String selection = "username=?";
        String[] selectionArgs = {username};
        values.put(KEY_TOKEN, hashToken);

        editor.putString(KEY_TOKEN, hashToken);
        editor.commit();

        db = openDB();
        db.update(TABLE_NAME, values, selection, selectionArgs);
        db.close();
    }

    public boolean validateSession() {

        String remember_token = pref.getString(KEY_TOKEN, "");
        boolean isSuccess = false;

        if (!remember_token.isEmpty()) {
            int count = 0;
            String[] columns = {"username"};
            String selection = "remember_token=?";
            String[] selectionArgs = {remember_token};

            db = openDB();
            cursor = db.query(TABLE_NAME, columns, selection, selectionArgs, null, null, null);
            count = cursor.getCount();
            cursor.close();
            db.close();

            isSuccess = count > 0;
        }
        return isSuccess;
    }

    public boolean checkUsername(String username) {
        int count = 0;
        String[] columns = {"username"};
        String selection = "username=?";
        String[] selectionArgs = {username};

        db = openDB();
        cursor = db.query(TABLE_NAME, columns, selection, selectionArgs, null, null, null);
        count = cursor.getCount();
        cursor.close();
        db.close();

        return count > 0;
    }

}
