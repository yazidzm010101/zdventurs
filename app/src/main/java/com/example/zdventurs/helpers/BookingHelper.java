package com.example.zdventurs.helpers;

import android.content.ContentValues;
import android.content.Context;

import com.example.zdventurs.models.Booking;

import java.util.ArrayList;

public class BookingHelper extends DatabaseHelper {
    final String TABLE_NAME = "bookings";

    public BookingHelper(Context ctx) {
        super(ctx);
    }

    public ArrayList<Booking> getAll(String userId) {
        String[] columns = {"id", "user", "departure", "destination", "timestamp", "adults", "childs", "price"};
        String whereClause = "user=?";
        String[] whereArgs = {userId};
        ArrayList<Booking> bookings = new ArrayList<>();
        db = openDB();
        cursor = db.query(TABLE_NAME, columns, whereClause, whereArgs, null, null, "timestamp DESC");
        while(cursor.moveToNext()) {
            Booking booking = new Booking(
                    cursor.getLong(0),
                    cursor.getLong(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getLong(4),
                    cursor.getInt(5),
                    cursor.getInt(6),
                    cursor.getInt(7)
            );
            bookings.add(booking);
        }
        cursor.close();
        db.close();
        return bookings;
    }

    public boolean create(Booking booking) {
        ContentValues values = new ContentValues();
        values.put("user", booking.getUser());
        values.put("departure", booking.getDeparture());
        values.put("destination", booking.getDestination());
        values.put("timestamp", booking.getTimestamp());
        values.put("adults", booking.getAdults());
        values.put("childs", booking.getChilds());
        values.put("price", booking.getPrice());

        db = openDB();
        long id = db.insert(TABLE_NAME, null, values);
        db.close();
        return id > 0;
    }

    public boolean delete(String id) {
        String whereClause = "id=?";
        String[] whereArgs = {id};
        db = openDB();
        long res = db.delete(TABLE_NAME, whereClause, whereArgs);
        db.close();
        return res > 0;
    }

}
