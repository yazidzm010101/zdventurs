package com.example.zdventurs.models;

public class Booking {
    private final long id;
    private final long user;
    private final String departure;
    private final String destination;
    private final long timestamp;
    private final int adults;
    private final int childs;
    private final int price;

    public Booking(long id, long user, String departure, String destination, long timestamp, int adults, int childs, int price) {
        this.id = id;
        this.user = user;
        this.departure = departure;
        this.destination = destination;
        this.timestamp = timestamp;
        this.adults = adults;
        this.childs = childs;
        this.price = price;
    }

    public long getId() { return id; }

    public long getUser() {
        return user;
    }

    public String getDeparture() {
        return  departure;
    }

    public String getDestination() {
        return destination;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public int getChilds() {
        return childs;
    }

    public int getAdults() {
        return adults;
    }

    public int getPrice() {
        return price;
    }
}
