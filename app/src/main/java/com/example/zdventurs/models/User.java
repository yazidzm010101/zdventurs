package com.example.zdventurs.models;

public class User {
    private final long id;
    private final String name;
    private final String username;
    private final String password;

    public User(long id, String name, String username, String password) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.password = password;
    }

    public long getId() { return id; }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
